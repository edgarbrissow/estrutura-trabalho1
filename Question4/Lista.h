#ifndef LISTA_H
#define LISTA_H

#ifndef NULL
#define NULL   ((void *) 0)
#endif

#include "NodoDuplo.h"
#include <iostream>
template <typename T>
class Lista{
private:
    int numeroElementos;
    Nodo<T>* inicio, *fim;
public:
    Lista(){
        inicio = NULL;
        fim = NULL;
        numeroElementos = 0;
    };

    ~Lista(){
        while(inicio != NULL){
            Nodo<T>* aux = inicio;
            inicio = inicio->getProximo();
            delete aux;
        }
        fim = inicio;

    };

    bool ehVazia(){
        return numeroElementos == 0;
    };

    int numeroDeElementos(){
        return numeroElementos;
    };

    bool existeElemento(T elemento){
        Nodo<T>* aux;
        aux = inicio;
        while(aux != NULL){
            if(aux->getElemento() == elemento)
                return true;
            aux = aux->getProximo();
        }
        return false;
    };

    T umElemento(int posicao){
        Nodo<T>* aux = inicio;
        unsigned count = 1;
        while(aux != NULL){
            if(count == posicao)
                return aux->getElemento();
            aux = aux->getProximo();
            count++;
        }
        return 0;

    };

    void insereNoInicio( T elemento){
        if(inicio == NULL){
            inicio = new Nodo<T>();
            inicio->setElemento(elemento);
            inicio->setAnterior(NULL);
            inicio->setProximo(NULL);
            fim = inicio;
            numeroElementos++;
        }else{
            Nodo<T>* aux = new Nodo<T>();
            aux->setElemento(elemento);
            aux->setProximo(inicio);
            inicio->setAnterior(aux);
            aux->setAnterior(NULL);
            inicio = aux;
            numeroElementos++;
        }
    };

    void insereNoFinal(T elemento){
        if(fim == NULL){
            inicio = new Nodo<T>();
            inicio->setElemento(elemento);
            inicio->setAnterior(NULL);
            inicio->setProximo(NULL);
            fim = inicio;
            numeroElementos++;
        }else{
            Nodo<T>* aux = new Nodo<T>();
            aux->setElemento(elemento);
            aux->setAnterior(fim);
            fim->setProximo(aux);
            aux->setProximo(NULL);
            fim = aux;
            numeroElementos++;
        }
    };

    void retiraDoInicio(){
        if(fim == inicio){
            delete fim;
            fim = inicio = NULL;
            numeroElementos--;
        }else {
            if (inicio != NULL) {
                Nodo<T> *aux = inicio;
                inicio = inicio->getProximo();
                if (inicio != NULL)
                    inicio->setAnterior(NULL);
                delete aux;
                numeroElementos--;
            }
        }
    };

    void retiraDoFinal(){
        if(fim == inicio){
            delete fim;
            fim = inicio = NULL;
            numeroElementos--;
        }else{
            if(fim != NULL){
                Nodo<T>* aux = fim;
                fim = fim->getAnterior();
                if(fim != NULL)
                    fim->setProximo(NULL);
                delete aux;
                numeroElementos--;
            }
        }
    };

    void retiraElemento(T elemento){
        Nodo<T>* aux = inicio;
        if(inicio != NULL) {
            if (inicio->getElemento() == elemento) {
                inicio = inicio->getProximo();
                inicio->setAnterior(NULL);
                delete aux;
                numeroElementos--;
            } else {
                if (fim->getElemento() == elemento) {
                    Nodo<T> *f = fim;
                    fim = fim->getAnterior();
                    fim->setProximo(NULL);
                    delete fim;
                    numeroElementos--;

                } else {
                    while (aux != NULL) {
                        if (aux->getElemento() == elemento) {
                            Nodo<T> *ant = aux->getAnterior();
                            Nodo<T> *prox = aux->getProximo();
                            ant->setProximo(prox);
                            prox->setAnterior(ant);
                            delete aux;
                            numeroElementos--;
                        }
                        aux = aux->getProximo();
                    }
                }
            }
        }




    };

    void mostra(){

        Nodo<T>* aux = inicio;

        while(aux != NULL){
            std::cout << "Elemento " << aux->getElemento() << std::endl;
            aux = aux->getProximo();

        }
    };
};


#endif //LISTA_H
