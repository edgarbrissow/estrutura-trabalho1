#ifndef NODODUPLO_H
#define NODODUPLO_H

template <typename T>
class Nodo{
public:
    Nodo(){};
    ~Nodo(){};
    void setElemento(T elemento){
        _elemento = elemento;
    };
    T getElemento(){
        return _elemento;
    };
    Nodo* getProximo(){
         return _proximo;
    };
    Nodo* getAnterior(){
        return _anterior;
    };
    void setProximo(Nodo* nodo){
        _proximo = nodo;
    };
    void setAnterior(Nodo* nodo){
        _anterior = nodo;
    };
private:
    T _elemento;
    Nodo* _proximo;
    Nodo* _anterior;

};

#endif //NODODUPLO_H
