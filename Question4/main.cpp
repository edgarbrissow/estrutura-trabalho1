#include <iostream>
#include "Lista.h"
int main() {
    Lista<int> l;
    l.insereNoInicio(0);
    l.insereNoInicio(1);
    l.insereNoFinal(2);
    for(int i=1; i <= l.numeroDeElementos(); i++)
    std::cout << "ELEMENTO " << l.umElemento(i) << std::endl;
    return 0;
}