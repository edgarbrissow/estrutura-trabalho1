#include <iostream>
#include <time.h>
int countChar(char  mat[][10], int line, int start, int size){
	if(start == size-1)
		return 0;
	
	if(mat[line][start] == mat[line][start+1])
	       return 1 + countChar(mat, line, start+1, size);	
	else
		return countChar(mat, line, start+1, size);
}

int main(){
	
	char mat[10][10];
	srand(time(NULL));

	for(int i=0; i < 10; i++){
		std::cout << std::endl;
		for(int j = 0; j < 10; j++){
			mat[i][j] = rand() % 25 + 97;
			std::cout << mat[i][j];
		}	
	}

	std::cout << std::endl <<countChar(mat, 0, 0, 10) << std::endl;
	std::cout << std::endl <<countChar(mat, 1, 0, 10) << std::endl;
	std::cout << std::endl <<countChar(mat, 2, 0, 10) << std::endl;
	std::cout << std::endl <<countChar(mat, 3, 0, 10) << std::endl;
	std::cout << std::endl <<countChar(mat, 4, 0, 10) << std::endl;
	std::cout << std::endl <<countChar(mat, 5, 0, 10) << std::endl;


}